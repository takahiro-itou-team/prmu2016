#!/bin/bash  -x

SCRIPT_DIR=/root/.scripts

apt  update  &&  \
apt  -y  install  git  unzip  emacs  vim

if test -f ${SCRIPT_DIR}/install-g++.sh ; then
    /bin/bash  -x  ${SCRIPT_DIR}/install-g++.sh
fi

if test -f ${SCRIPT_DIR}/install-python.sh ; then
    /bin/bash  -x  ${SCRIPT_DIR}/install-python.sh
fi

