
FROM  ubuntu

ADD  ./files  /root

RUN  /bin/bash  -x  /root/.scripts/setup.sh

WORKDIR  /root/alcon2016

RUN  mkdir  VirtualDatasets  &&                       \
     unzip  ../zip/DatasetGenerator.zip             ; \
     unzip  ../zip/SampleCodes1_1.zip               ; \
     unzip  -d VirtualDatasets/  ../zip/Level1.zip  ; \
     unzip  -d VirtualDatasets/  ../zip/Level2.zip

