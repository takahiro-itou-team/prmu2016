
USER_NAME    = takahiroitou
PROJECT_NAME = prmu2016-environment
PACKAGE_NAME = ${USER_NAME}/${PROJECT_NAME}

SOURCE_DIR   = .
DOCKER_CMD   = docker

build:
	${DOCKER_CMD}  build  -t  ${PACKAGE_NAME} ${SOURCE_DIR}

